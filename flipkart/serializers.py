from rest_framework import serializers
from flipkart.models import Products,Orders,OrdersItems

class ProductSerializer(serializers.ModelSerializer):
    class meta:
        model=Products
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    class meta:
        model=Orders
        fields = '__all__'

class OrdersItemsSerializer(serializers.ModelSerializer):
    class meta:
        model=OrdersItems
        fields = '__all__'
