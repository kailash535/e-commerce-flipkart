from flipkart.models import Products
from rest_framework import viewsets,permissions
from flipkart.serializers import ProductSerializer

class ProductViewSet(viewsets.ModelViewSet):
    permissions_classes = [
        permissions.AllowAny
    ]
    Serializer_class = ProductSerializer

