from flipkart.models import Orders
from rest_framework import viewsets,permissions
from flipkart.serializers import OrderSerializer

class OrdersViewSet(viewsets.ModelViewSet):
    permissions_classes = [
        permissions.AllowAny
    ]
    Serializer_class = OrderSerializer