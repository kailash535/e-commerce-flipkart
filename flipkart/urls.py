
from rest_framework import routers
from .product_api import ProductViewSet
from .orders import OrdersViewSet

router =routers.DefaultRouter()
router.register('api/products',ProductViewSet,'flipkart')
router.register('api/orders',OrdersViewSet,'flipkart')

urlpatterns = router.urls